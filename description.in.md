# ${OC_INFO_IMAGE_REFNAME}

[![](https://images.microbadger.com/badges/version/${DOCKER_NAMESPACE}/${OC_INFO_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${OC_INFO_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/image/${DOCKER_NAMESPACE}/${OC_INFO_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${OC_INFO_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/license/${DOCKER_NAMESPACE}/${OC_INFO_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${OC_INFO_IMAGE_REFNAME} "Get your own license badge on microbadger.com") [![](https://img.shields.io/badge/Gitlab-source-blue.svg)](${CI_PROJECT_URL})

## ${OC_INFO_IMAGE_TITLE}

see [${CI_PROJECT_PATH}](${CI_PROJECT_URL}),

 Custom build image for [oc-runner](http://oc-runner.deployctl.com/latest).

with `make_os.conf`:

```bash

${MAKE_OS_CONFIG}

```

and `docker_config.yml`:

```yaml

${DOCKER_CONFIG_YML}

```
